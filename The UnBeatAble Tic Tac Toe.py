from random import randrange
import random

#---------------------------------------------------------------------------# making a list for the tic tac toes list spot:

ttt = [ 0 , 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 ] # a list for the game list
list1 = [ 0 , 2 , 6 , 8 ] # a list for diagnols ( this will be used for pre attacks )
list2 = [ 1 , 3 , 5 , 7 ] # a list for sides ( this will be used for pre attacks )

#---------------------------------------------------------------------------# writing a function for drawing a list for our game:

def gamelist():
    print( " {} | {} | {} " .format( ttt[ 0 ] , ttt[ 1 ] , ttt[ 2 ] )) # row 1. numbers.
    print( " --+---+-- " ) # row 1. lines.
    print( " {} | {} | {} " .format( ttt[ 3 ] , ttt[ 4 ] , ttt[ 5 ] )) # row 2. numbers.
    print( " --+---+-- " ) # row 2. lines.       
    print( " {} | {} | {} " .format( ttt[ 6 ] , ttt[ 7 ] , ttt[ 8 ] )) # row 3. numbers.
    print( "===========" )   

#---------------------------------------------------------------------------# writing a function to check if computer won:

def winnercom():

    #--------------------------# cheking if the are 3 X/O in one row :       
        
    if ttt[0] == "O" and ttt[1] == "O" and ttt[2] == "O": # checking if the player won. row 1.
        return 1

    elif ttt[3] == "O" and ttt[4] == "O" and ttt[5] == "O": # checking if the player won. row 2.
        return 1
        
    elif ttt[6] == "O" and ttt[7] == "O" and ttt[8] == "O": # checking if the player won. row 3.
        return 1

    #--------------------------# cheking if the are 3 X/O in one cullom :

    elif ttt[0] == "O" and ttt[3] == "O" and ttt[6] == "O": # checking if the player won. cullom 1.
        return 1 

    elif ttt[1] == "O" and ttt[4] == "O" and ttt[7] == "O": # checking if the player won. cullom 2.
        return 1 

    elif ttt[2] == "O" and ttt[5] == "O" and ttt[8] == "O": # checking if the player won. cullom 3.
        return 1

    #--------------------------# cheking if the are 3 X/O in one diagnol :

    elif ttt[0] == "O" and ttt[4] == "O" and ttt[8] == "O": # checking if the player won. diagnol 1.
        return 1

    elif ttt[2] == "O" and ttt[4] == "O" and ttt[6] == "O": # checking if the player won. diagnol 2.
        return 1

    #--------------------------# 

    else: 
        return 0

#---------------------------------------------------------------------------# writing a function to check if player won ( this will never happen but it is wroten ):

def winner():

    #--------------------------# cheking if the are 3 X/O in one row :       
        
    if ttt[0] == "X" and ttt[1] == "X" and ttt[2] == "X": # checking if the player won. row 1.
        return 2

    elif ttt[3] == "X" and ttt[4] == "X" and ttt[5] == "X": # checking if the player won. row 2.
        return 2
        
    elif ttt[6] == "X" and ttt[7] == "X" and ttt[8] == "X": # checking if the player won. row 3.
        return 2

    #--------------------------# cheking if the are 3 X/O in one cullom :

    elif ttt[0] == "X" and ttt[3] == "X" and ttt[6] == "X": # checking if the player won. cullom 1.
        return 2

    elif ttt[1] == "X" and ttt[4] == "X" and ttt[7] == "X": # checking if the player won. cullom 2.
        return 2

    elif ttt[2] == "X" and ttt[5] == "X" and ttt[8] == "X": # checking if the player won. cullom 3.
        return 2

    #--------------------------# cheking if the are 3 X/O in one diagnol :

    elif ttt[0] == "X" and ttt[4] == "X" and ttt[8] == "X": # checking if the player won. diagnol 1.
        return 2

    elif ttt[2] == "X" and ttt[4] == "X" and ttt[6] == "X": # checking if the player won. diagnol 2.
        return 2

    #--------------------------# # the player has not yet won and breaks out of the function and continues

    else:
        return 2

#---------------------------------------------------------------------------# writing a function for asking in which slot put X/O:

def player1():
    # getting an input from the player for where to place X/O in the list
    pos = int( input ( "Where Do You Want To Put Your X: " ) )

    if pos in list1: # if the input from player was in list1, remove the number from list1
        list1.remove( pos ) 
           
    elif pos in list2: # if the input from player was in list2, remove the number from list2
        list2.remove( pos ) 

    if pos <=8 and pos >=0: # checks if the enterd numbers is from 0 to 8.
            
        if ttt[ pos ] == "X" or ttt[ pos ] == "O": # checks if the enterd number slot is already taken.
            print ( "This Slot Is Already Taken. Please Try Again." )
            player1()
            
        else: # check if the enterd number slot is not already taken.
            ttt[ pos ] = "X"
            gamelist()
        
    else: # checks if the enterd numbers is not from 0 to 8.
        print( "!Invalid Number! Please Enter A number from 0 to 8." )  
        player1()

#---------------------------------------------------------------------------# writing a function for attacking or pre attack:

def attack(x):

    #--------------------------# checking if the game can be ended by the computer in rows

    # row 1:     
    if ( ttt[ 0 ] == "O" and ttt[ 1 ] == "O" and ttt[ 2 ] == 2 ) or ( ttt[ 0 ] == "O" and ttt[ 2 ] == "O" and ttt[ 1 ] == 1 ) or ( ttt[ 1 ] == "O" and ttt[ 2 ] == "O" and ttt[ 0 ] == 0 ):      
        
        if ttt[ 0 ] == 0:
            ttt[ 0 ] = "O"

        elif ttt[ 1 ] == 1:
            ttt[ 1 ] = "O"

        elif ttt[ 2 ] == 2:
            ttt[ 2 ] = "O"

    # row 2:

    elif ( ttt[ 3 ] == "O" and ttt[ 4 ] == "O" and ttt[ 5 ] == 5 ) or ( ttt[ 3 ] == "O" and ttt[ 5 ] == "O" and ttt[ 4 ] == 4 ) or ( ttt[ 4 ] == "O" and ttt[ 5 ] == "O" and ttt[ 3 ] == 3 ):      
        
        if ttt[ 3 ] == 3:
            ttt[ 3 ] = "O"

        elif ttt[ 4 ] == 4:
            ttt[ 4 ] = "O"

        elif ttt[ 5 ] == 5:
            ttt[ 5 ] = "O"

    # row 3

    elif ( ttt[ 6 ] == "O" and ttt[ 7 ] == "O" and ttt[ 8 ] == 8 ) or ( ttt[ 6 ] == "O" and ttt[ 8 ] == "O" and ttt[ 7 ] == 7 ) or ( ttt[ 7 ] == "O" and ttt[ 8 ] == "O" and ttt[ 6 ] == 6 ):      
        
        if ttt[ 6 ] == 6:
            ttt[ 6 ] = "O"

        elif ttt[ 7 ] == 7:
            ttt[ 7 ] = "O"

        elif ttt[ 8 ] == 8:
            ttt[ 8 ] = "O"

    #--------------------------# checking if the game can be ended by the computer in collums

    # collum 1:
         
    elif ( ttt [ 0 ] == "O" and ttt[ 3 ] == "O" and ttt[ 6 ] == 6 ) or ( ttt[ 0 ] == "O" and ttt[ 6 ] == "O" and ttt[ 3 ] == 3 ) or ( ttt[ 3 ] == "O" and ttt[ 6 ] == "O" and ttt[ 0 ] == 0 ):      
        
        if ttt[ 0 ] == 0:
            ttt[ 0 ] = "O"

        elif ttt[ 3 ] == 3:
            ttt[ 3 ] = "O"

        elif ttt[ 6 ] == 6:
            ttt[ 6 ] = "O"

    # collum 2:
    
    elif ( ttt[ 1 ] == "O" and ttt[ 4 ] == "O" and ttt[ 7 ] == 7 ) or ( ttt[ 1 ] == "O" and ttt[ 7 ] == "O" and ttt[ 4 ] == 4 ) or ( ttt[ 4 ] == "O" and ttt[ 7 ] == "O" and ttt[ 1 ] == 1 ):      
        
        if ttt[ 1 ] == 1:
            ttt[ 1 ] = "O"

        elif ttt[ 4 ] == 4:
            ttt[ 4 ] = "O"

        elif ttt[ 7 ] == 7:
            ttt[ 7 ] = "O"

    # collum 3 

    elif ( ttt[ 2 ] == "O" and ttt[ 5 ] == "O" and ttt[ 8 ] == 8 ) or ( ttt[ 2 ] == "O" and ttt[ 8 ] == "O" and ttt[ 5 ] == 5 ) or ( ttt[ 5 ] == "O" and ttt[ 8 ] == "O" and ttt[ 2 ] == 2 ):      
         
        if ttt[ 2 ] == 2:
            ttt[ 2 ] = "O"

        elif ttt[ 5 ] == 5:
            ttt[ 5 ] = "O"

        elif ttt[ 8 ] == 8:
            ttt[ 8 ] = "O"

    #--------------------------# checking if the game can be ended by the computer in diagnols

    # diagnol left to right:
         
    elif ( ttt[ 0 ] == "O" and ttt[ 4 ] == "O" and ttt[ 8 ] == 8 ) or ( ttt[ 0 ] == "O" and ttt[ 8 ] == "O" and ttt[ 4 ] == 4 ) or ( ttt[ 4 ] == "O" and ttt[ 8 ] == "O" and ttt[ 0 ] == 0 ):      
        
        if ttt[ 0 ] == 0:
            ttt[ 0 ] = "O"

        elif ttt[ 4 ] == 4:
            ttt[ 4 ] = "O"

        elif ttt[ 8 ] == 8:
            ttt[ 8 ] = "O"

    # diagnol right to left:
         
    elif ( ttt[ 2 ] == "O" and ttt[ 4 ] == "O" and ttt[ 6 ] == 6 ) or ( ttt[ 2 ] == "O" and ttt[ 6 ] == "O" and ttt[ 4 ] == 4 ) or ( ttt[ 4 ] == "O" and ttt[ 6 ] == "O" and ttt[ 2 ] == 2 ):      
        
        if ttt[ 2 ] == 2:
            ttt[ 2 ] = "O"

        elif ttt[ 4 ] == 4:
            ttt[ 4 ] = "O"

        elif ttt[ 6 ] == 6:
            ttt[ 6 ] = "O"

    # pre attack 1:

    elif x == 3:

        if ( ttt[ 0 ] == "O" and ttt[ 8 ] == 8 ) or ( ttt[ 8 ] == "O" and ttt[ 0 ] == 0 ):

            if ttt[ 0 ] == 0:
                ttt[ 0 ] = "O"
                list1.remove( 0 )

            elif ttt[ 8 ] == 8:
                ttt[ 8 ] = "O"
                list1.remove( 8 )

        elif ( ttt[ 2 ] == "O" and ttt[ 6 ] == 6 ) or ( ttt[ 6 ] == "O" and ttt[ 2 ] == 2 ):

            if ttt[ 2 ] == 2:
                ttt[ 2 ] = "O"
                list1.remove( 2 )

            elif ttt[ 6 ] == 6:
                ttt[ 6 ] = "O"
                list1.remove( 6 )

        else:    
            movement = random.choice( list1 )
            ttt[ movement ] = "O"
            list1.remove( movement )

    # pre attack 2:

    elif x == 9:    

        for i in range( 9 ):

            if ttt[ i ] == i:
                ttt[ i ] = "O"

    # pre attack 3:

    elif x == 2:
        
        if ttt[ 4 ] == 4:
            ttt[ 4 ] = "O"

        else:
            movement = random.choice( list1 )
            ttt[ movement ] = "O"
            list1.remove( movement )  

    # if none of the any ifs where true

    else:
        defend(x)

#---------------------------------------------------------------------------# writing a function for defending:

def defend(x):

    #--------------------------# checking if the game can be ended by the computer in rows

    # row 1:     
    if ( ttt[ 0 ] == "X" and ttt[ 1 ] == "X" and ttt[ 2 ] == 2 ) or ( ttt[ 0 ] == "X" and ttt[ 2 ] == "X" and ttt[ 1 ] == 1 ) or ( ttt[ 1 ] == "X" and ttt[ 2 ] == "X" and ttt[ 0 ] == 0 ):      
        
        if ttt[ 0 ] == 0:
            ttt[ 0 ] = "O"

        elif ttt[ 1 ] == 1:
            ttt[ 1 ] = "O"

        elif ttt[ 2 ] == 2:
            ttt[ 2 ] = "O"

    # row 2:

    elif ( ttt [ 3 ] == "X" and ttt [ 4 ] == "X" and ttt [ 5 ] == 5 ) or ( ttt [ 3 ] == "X" and ttt [ 5 ] == "X" and ttt [ 4 ] == 4 ) or ( ttt [ 4 ] == "X" and ttt [ 5 ] == "X" and ttt [ 3 ] == 3 ):      
        
        if ttt [ 3 ] == 3:
            ttt [ 3 ] = "O"

        elif ttt [ 4 ] == 4:
            ttt [ 4 ] = "O"

        elif ttt [ 5 ] == 5:
            ttt [ 5 ] = "O"

    # row 3

    elif ( ttt[ 6 ] == "X" and ttt[ 7 ] == "X" and ttt[ 8 ] == 8 ) or ( ttt[ 6 ] == "X" and ttt[ 8 ] == "X" and ttt[ 7 ] == 7 ) or ( ttt[ 7 ] == "X" and ttt[ 8 ] == "X" and ttt[ 6 ] == 6 ):      
        
        if ttt[ 6 ] == 6:
            ttt[ 6 ] = "O"

        elif ttt[ 7 ] == 7:
            ttt[ 7 ] = "O"

        elif ttt[ 8 ] == 8:
            ttt[ 8 ] = "O"

    #--------------------------# checking if the game can be ended by the computer in collums

    # collum 1:
         
    elif ( ttt[ 0 ] == "X" and ttt[ 3 ] == "X" and ttt[ 6 ] == 6 ) or ( ttt[ 0 ] == "X" and ttt[ 6 ] == "X" and ttt[ 3 ] == 3 ) or ( ttt[ 3 ] == "X" and ttt[ 6 ] == "X" and ttt[ 0 ] == 0 ):      
        
        if ttt[ 0 ] == 0:
            ttt[ 0 ] = "O"

        elif ttt[ 3 ] == 3:
            ttt[ 3 ] = "O"

        elif ttt[ 6 ] == 6:
            ttt[ 6 ] = "O"

    # collum 2:
    
    elif ( ttt[ 1 ] == "X" and ttt[ 4 ] == "X" and ttt[ 7 ] == 7 ) or ( ttt[ 1 ] == "X" and ttt[ 7 ] == "X" and ttt[ 4 ] == 4 ) or ( ttt[ 4 ] == "X" and ttt[ 7 ] == "X" and ttt[ 1 ] == 1 ):      
        
        if ttt[ 1 ] == 1:
            ttt[ 1 ] = "O"

        elif ttt[ 4 ] == 4:
            ttt[ 4 ] = "O"

        elif ttt[ 7 ] == 7:
            ttt[ 7 ] = "O"

    # collum 3 

    elif ( ttt[ 2 ] == "X" and ttt[ 5 ] == "X" and ttt[ 8 ] == 8 ) or ( ttt[ 3 ] == "X" and ttt[ 8 ] == "X" and ttt[ 5 ] == 5 ) or ( ttt[ 5 ] == "X" and ttt[ 8 ] == "X" and ttt[ 2 ] == 2 ):      
        
        if ttt[ 2 ] == 2:
            ttt[ 2 ] = "O"

        elif ttt[ 5 ] == 5:
            ttt[ 5 ] = "O"

        elif ttt[ 8 ] == 8:
            ttt[ 8 ] = "O"

    #--------------------------# checking if the game can be ended by the computer in diagnols

    # diagnol left to right:
         
    elif ( ttt[ 0 ] == "X" and ttt[ 4 ] == "X" and ttt[ 8 ] == 8 ) or ( ttt[ 0 ] == "X" and ttt[ 8 ] == "X" and ttt[ 4 ] == 4 ) or ( ttt[ 4 ] == "X" and ttt[ 8 ] == "X" and ttt[ 0 ] == 0 ):      
        
        if ttt[ 0 ] == 0:
            ttt[ 0 ] = "O"

        elif ttt[ 4 ] == 4:
            ttt[ 4 ] = "O"

        elif ttt[ 8 ] == 8:
            ttt[ 8 ] = "O"

    # diagnol right to left:
         
    elif ( ttt[ 2 ] == "X" and ttt[ 4 ] == "X" and ttt[ 6 ] == 6 ) or ( ttt[ 2 ] == "X" and ttt[ 6 ] == "X" and ttt[ 4 ] == 4 ) or ( ttt[ 4 ] == "X" and ttt[ 6 ] == "X" and ttt[ 2 ] == 2 ):      
        
        if ttt[ 2 ] == 2:
            ttt[ 2 ] = "O"

        elif ttt[ 4 ] == 4:
            ttt[ 4 ] = "O"

        elif ttt[ 6 ] == 6:
            ttt[ 6 ] = "O"
    
    # pre defend:

    elif x == 5:

        movement = random.choice( list2 )
        ttt[ movement ] = "O"
        list2.remove( movement ) 

    # if none of the ifs were true:   
    
    else:
        movement = random.choice( list1 )
        ttt[ movement ] = "O"

#---------------------------------------------------------------------------# computer goes first and the player goes second

def computer():

    print( "Alright! You Are X and The Compter Is O! Let The Game Begin!" )

    #--------------------------# first step ( computer )   

    movement = random.choice( list1 )
    ttt[ movement ] = "O"
    list1.remove( movement )    

    #--------------------------# second step ( player )

    gamelist()
    player1()

    #--------------------------# third step ( computer )      

    attack(3) # calling a pre attack
    
    #--------------------------# fourth step ( player )

    gamelist()
    player1()

    #--------------------------# fifth step ( computer ) the game can end here

    attack(1) # calling an attack
    
    if winnercom():
        print ( "Computer Won! You Lost! Press CTRL + F5 To Play Again, Final Results:" )
        gamelist()

    else:
        #--------------------------# sixth step ( player )
        
        gamelist()
        player1()    

        #--------------------------# seventh step ( computer ) the game can end here     

        attack(1) # calling an attack
        
        if winnercom():
            print ( "Computer Won! You Lost! Press CTRL + F5 To Play Again, Final Results:" )
            gamelist()

        else:
            #--------------------------# eighth step ( player )

            gamelist ()
            player1 ()

            #--------------------------# nineth step ( computer ) the game can end here

            attack(9) # calling a pre attack 
            if not winnercom():
                print ( "Tied! Press CTRL + F5 To Play Again" )

            elif winnercom():
              
                print ( "Computer Won! You Lost! Press CTRL + F5 To Play Again, Final Results:" ) 

            gamelist()    

#---------------------------------------------------------------------------# player goes first and the computer goes second

def player():

    print( "Alright! You Are X and The Compter Is O! Let The Game Begin!" )

    #--------------------------# first step ( player )

    gamelist()
    player1()

    #--------------------------# second step ( computer )

    attack(2) # calling a pre attack

    #--------------------------# third step ( player )

    gamelist()
    player1()

    #--------------------------# fourth step ( computer )

    attack(5) # calling a pre attack

    #--------------------------# fifth step ( player )

    gamelist()
    player1()

    #--------------------------# sixth step ( computer )

    attack(1) # calling an attack

    if winnercom():
        print( "Computer Won! You Lost! Press CTRL + F5 To Play Again, Final Results:" )
        gamelist()

    #--------------------------# seventh step ( player )

    else:
        gamelist()
        player1()

        #--------------------------# eighth step ( computer )

        attack(1) # calling an attack

        if winnercom():
            print ( "Computer Won! You Lost! Press CTRL + F5 To Play Again, Final Results:" )
            gamelist()

        #--------------------------# nineth step ( player )

        else:
            gamelist()
            player1()

            if not winnercom ():
                print ( "Tied! Press CTRL + F5 To Play Again, Final Results:" )

            elif winnercom():
                print ( "Computer Won! You Lost! Press CTRL + F5 To Play Again, Final Results:" )
        
            gamelist()   

#---------------------------------------------------------------------------# program start and information ( the information was written in diffrent print functions to make it easier to read and unerestand)

print( "Hello And Welcome To Tic Tac Toe!" )
print( "How To Play: At The Start Game List Will Be Printed In Your Terminal And There Are Nine Slots Labeld.")
print( "In The Terminal When It Is Your Turn The Program Will Ask You Where Do You Want To Put Your Symbol." )
print( "And The Goal Of This Game Is To Have 3 of Your Symbols In a Row But..." )
print( "This Program Was Written In a Way That It Is Impossible To Beat!" )

choosen = input( "Anyways Let's Start! Who You Want To Go First? For (You) Type 1, For (Computer) type 2:" )

while (True):

    if choosen == "1":
        player()
        break

    elif choosen == "2":
        computer()
        break

    else:
        choosen = input( "Invalid Number! Please Try Again. Who You Want To Go First? For (You) Type 1, For (Computer) type 2:" )
